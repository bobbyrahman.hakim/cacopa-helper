CACOPA Helper 
=======================

CACOPA Helper is Library for PHP / Laravel, You can use CACOPA Helper anytime and anywhere for your code. 

if you have any idea and if you need develop this library, you can commit and push this repo and I Appreciate for you time for develop or using CACOPA HELPER.

Just install the package, add the config and it is ready to use!


Requirements
============

* php: >=5.3.9
* Predis >1.1.9

Installation
============

    composer require cacopa/helper:dev-main

Add the service provider and facade in your config/app.php

Service Provider
    Cacopa\Helper

Facade

    'CacopaHelper'            => 'Cacopa\Helper',


Usage
=====

Set Key and Save Redis

    CacopaHelper\PredisCustom::SetRedis("KeyCoba","this is content cache",7200); // param : key, content, expired_cache
    
Get Content cache redis with Key.

    CacopaHelper\PredisCustom::GetRedis("KeyCoba");



Credits
=======

* Powered By ZAKIZAID
* Developer By Bobby Rahman H.