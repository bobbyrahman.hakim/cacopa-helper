<?php
namespace Cacopa\Helper;

include_once "PredisCustom.php";

class ConfigCustomSaveTxt extends PredisCustom 
{
    public $customVar = [];

    public function __construct($arrx) {
		$this->customVar = $arrx;
	}

	/*
		$configarr = [ 
			"nameconfig" => "Chart-Rqa-ColorChart",
			"data"=>[
				[
					"title" => "Question Color",
					"default" => "#876445",
					"attribut" =>"",
					"type" =>"color"
				],
				[
					"title" => "User Question",
					"default" => "#1BD824",
					"attribut" =>"",
					"type" =>"color"
				],
				[
					"title" => "Question Related Answers",
					"default" => "#111399",
					"attribut" =>"",
					"type" =>"color"
				],
				[
					"title" => "User Related Answers",
					"default" => "#0CA881",
					"attribut" =>"",
					"type" =>"color"
				],
				[
					"title" => "Question Random Answers",
					"default" => "#0CA881",
					"attribut" =>"",
					"type" =>"color"
				],
				[
					"title" => "User Random Answers",
					"default" => "#5C4BBB",
					"attribut" =>"",
					"type" =>"color"
				],
			]
		];
	*/
	public function setConfig($arrx){ 
		$a = new ConfigCustomSaveTxt($arrx);
		return $a;
	}

	public function getConfig(){
		return $this->customVar;
	}

    public function paramTxtField($name = "",$types = "text",$values="", $attribut="")
    {		
        if(isset($name)){
            return '<label for = "'.$name.'">'.ucfirst($name).'</label> 
            <input 
            value = "'.$values.'" 
            type = "'.$types.'" 
            name = "'.str_replace(" ","",strtolower($name)).'" 
            '.$attribut.' />'; 
        }
        return "";
    }
    
	public function showConfigCache(){
		$CRedisNull = self::CheckRedisNull($this->customVar['nameconfig']);
		if($CRedisNull){
			self::SetRedis($this->customVar['nameconfig'], $this->customVar['data'], 172800);
			
		}else{
			$this->customVar['data'] = self::GetRedis($this->customVar['nameconfig']);
		}
		return $this->customVar['data'];
	}

	public function showForm(){
		self::showConfigCache();
		foreach ($this->customVar['data'] as $a => $b) {
			$attr = (isset($b['attribut']))? $b['attribut'] : "";
			$res[]['html'] =$this->paramTxtField($b['title'],$b['type'],$b['default'],$attr);
		}
		return $res;
	}

	public function saveForm($data){
		$res = [];
		if(isset($data)){
			foreach ($this->customVar['data'] as $a => $b) {
				$str = str_replace(" ","",strtolower($b['title']));
				$regexTitle = preg_replace('/\W+/',"", $str);
				$res[$a]['title'] =$b['title'];
				$res[$a]['type'] =$b['type'];
				$res[$a]['default'] =(isset($data[$regexTitle])) ? $data[$regexTitle] : $b['default'];
			}
			self::SetRedis($this->customVar['nameconfig'], $res, 172800);
			return true;
		}else{
			return false;
		}
			
	}
}