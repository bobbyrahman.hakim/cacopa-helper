<?php
namespace Cacopa\Helper;
 
use Predis\predis;

class PredisCustom
{
    public $connectStatus = false;

    public function __construct($status) {
            $this->connectStatus = $status;
    }

    private function ConfigRedis($key=""){
        $cfg["scheme"] =SCHEME_REDIS;
        $cfg["host"] =HOST_REDIS;
        $cfg["port"] =PORT_REDIS;
        $cfg["filesname"] =FILESNAME_LADOO;
        $cfg["expired"] =EXPIRE_SECOND_REDIS;
        $cfg["password"] =PASSWORD_REDIS;

        if($key == ""){
            return $cfg;
        }else{
            return $cfg[$key];
        }        
    }

    public function DBRedis(){    
        $passwordredis = self::ConfigRedis('password');
        if($passwordredis != ""){             
            $predis = new \Predis\Client([
                'scheme' => self::ConfigRedis('scheme'),
                'host'   => self::ConfigRedis('host'),
                'port'   => self::ConfigRedis('port'),
                'password'   => self::ConfigRedis('password'),
            ]);
        }else{                   
            $predis = new \Predis\Client([
                'scheme' => self::ConfigRedis('scheme'),
                'host'   => self::ConfigRedis('host'),
                'port'   => self::ConfigRedis('port'),
            ]);
        }
        return $predis;
    }

    
    public function ping(){   
        if(self::DBRedis()->ping() == 'PONG'){
           $a = new PredisCustom(true);
        }else{
            $a = new PredisCustom(false);
        }
        self::DBRedis()->disconnect();
        return $a->getConnected();
    }

    public function getConnected(){
        return $this->connectStatus;
    }

    public function SetRedis($namafile, $items, $exp=""){
        if(self::ping()){
            $cachename = self::ConfigRedis('filesname') . $namafile;
            $expired = ($exp == "")? self::ConfigRedis('expired') : $exp;
            $item = json_encode($items);
            self::DBRedis()->set($cachename, $item);
            self::DBRedis()->expire($cachename, $expired);
            self::DBRedis()->disconnect();
        }
    }
    
    public function GetRedis($namafile){
        $res=[];
        if(self::ping()){
            $cachename = self::ConfigRedis('filesname'). $namafile;
            $var = self::DBRedis()->get($cachename);
            $res =  json_decode($var,true);
            self::DBRedis()->disconnect();
            return $res;
        }
    }
    
    public function RemoveRedis($cachename){
        if(self::ping()){
            $var = self::DBRedis()->del($cachename);
            self::DBRedis()->disconnect();
            return true;
        }
    }

    public function GetAllKeys(){
        $res=[];
        if(self::ping()){
            $res = self::DBRedis()->keys('*');
            self::DBRedis()->disconnect();
            
        }
        return $res;
    }

    public function MemoryKeys($cachename){
        if(self::ping()){
            $var = self::DBRedis()->get($cachename);
            $content =  json_decode($var,true);
            self::DBRedis()->disconnect();
            $t = @mb_strlen(json_encode($content, JSON_NUMERIC_CHECK), '8bit');
            $bytes = $t+strlen($t);
            if ($bytes >= 1073741824) 
            { $bytes = number_format($bytes / 1073741824, 2) . ' GB'; }
            elseif ($bytes >= 1048576)
            { $bytes = number_format($bytes / 1048576, 2) . ' MB'; }
            elseif ($bytes >= 1024)
            { $bytes = number_format($bytes / 1024, 2) . ' KB'; }
            elseif ($bytes > 1)
            { $bytes = $bytes . ' bytes'; }
            elseif ($bytes == 1)
            { $bytes = $bytes . ' byte'; }
            else
            { $bytes = '0 bytes'; }
        }
        return $bytes;
    }

    public function FuncKeys($cachename,$method){
        $res=[];
        if(self::ping()){
            $res = self::DBRedis()->{$method}($cachename);
            self::DBRedis()->disconnect();            
        }
        return $res;
    }

    public function CheckRedisNull($namafile){
        if(self::ping()){
            $getRedis = self::GetRedis($namafile);
            $item =[];
            self::DBRedis()->disconnect();

            if(!isset($getRedis) && $getRedis == ""){
                return true;
            }else{
                return false;
            }
        }
    }
}